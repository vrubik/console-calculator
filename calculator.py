import math


def add(a, b):
    return a + b


def sub(a, b):
    return a - b


def mul(a, b):
    return a * b


def div(a, b):
    return a / b


def cot(x):
    return math.cos(x) / math.sin(x)


calc_object = [
    {
        '+': add,
        '-': sub,
        '*': mul,
        '/': div
    },
    {
        'sin': math.sin,
        'cos': math.cos,
        'tan': math.tan,
        'cot': cot
    }
]

if __name__ == '__main__':
    print('Hello world')
