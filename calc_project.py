import json
import os
import datetime
import urllib.request
from calculator import *

json_file = os.path.join('users_data.json')
auth_flag = False
date = str(datetime.date.today())
log = None
pas = ''

try:
    html = urllib.request.urlopen('https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5')
    data_html = html.read()
    json_object_html = json.loads(data_html)
except:
    print('Ошибка получения данных по API')


def get_log():
    # Запрашивает логин у пользователя
    return input('Login >>> ')


def get_pass():
    # Запрашивает пароль у пользователя
    return input('Password >>> ')


def get_num():
    # Запрашивает число с типом float
    return float(input('Введите число >>> '))


def validate_password(password):
    # Проверяет пароль на валидность
    count_upper_chars = 0
    count_spec_chars = 0
    spec_chars = '?!,.'
    for elem in password:
        if elem in spec_chars:
            count_spec_chars += 1
        if ord(elem) in range(ord('A'), ord('Z') + 1):
            count_upper_chars += 1
    if count_upper_chars < 2 and count_spec_chars < 1:
        return False
    else:
        return True


def get_simple_operation():
    # Формирует строку из символов простых мат-операций
    return input(' '.join(calc_object[0].keys()) + ' >>> ')


def get_trig_operation():
    # Формирует строку из названий тригонометрических операций
    return input(' '.join(calc_object[1].keys()) + ' >>> ')


def read_json(js_file):
    # Читает json файл и записывает его в переменную data, если файла нет то создает его
    try:
        data = json.load(open(js_file))
    except:
        data = []
    return data


def write_json(data_dict, list_data):
    # Добавляет новые данные и перезаписывает json файл
    list_data.append(data_dict)
    with open(json_file, 'w', encoding='UTF-8') as file:
        json.dump(list_data, file, indent=2, ensure_ascii=False)


def register():
    # Регистрирует нового пользователя, проверяет на наличие существующего логина
    global auth_flag, log, pas
    data = read_json(json_file)
    print('Пароль должен содержать минимум 2 большие буквы и минимум один спецсимвол (?!,.)')
    log = get_log()
    pas = get_pass()
    if len(data) == 0:
        if validate_password(pas):
            write_json(data_to_dict(login=log, password=pas, operation=[]), data)
            auth_flag = True
        else:
            print('Введенный пароль не является приемлемым.')
            auth_flag = False
    else:
        for dict_user in data:
            if dict_user['login'] == log:
                print('Пользователь с таким именем уже существует.')
                auth_flag = False
                break
            elif not validate_password(pas):
                print('Введенный пароль не является приемлемым.')
                auth_flag = False
                break
        else:
            write_json(data_to_dict(login=log, password=pas, operation=[]), data)
            auth_flag = True


def simple_calc():
    # Функция простого калькулятора, записывает операции если пользователь авторизирован
    print('< Для выхода из калькулятора введите вместо числа любую букву. >')
    global date
    data = read_json(json_file)
    while True:
        try:
            a = get_num()
            operation = get_simple_operation()
            b = get_num()
            if operation in calc_object[0]:
                print(round(calc_object[0][operation](a, b), 2))
                action = f"{a} {operation} {b} = {round(calc_object[0][operation](a, b), 2)}"
                for i in data:
                    if i['login'] == log:
                        i['operation'].append({date: action})
            else:
                print('Invalid operation')
            with open(json_file, 'w', encoding='utf-8') as file:
                json.dump(data, file, indent=2, ensure_ascii=False)

        except ValueError:
            break


def trigonometric_calc():
    # Функция тригонометрического калькулятора, записывает операции если пользователь авторизирован
    print('< Для выхода из калькулятора введите вместо числа любую букву. >')
    global date
    data = read_json(json_file)
    while True:
        try:
            operation = get_trig_operation()
            x = get_num()
            if operation in calc_object[1]:
                print(f'{operation}({x}) = {calc_object[1][operation](x)}')
                action = f"{operation} {x} = {calc_object[1][operation](x)}"
                for i in data:
                    if i['login'] == log:
                        i['operation'].append({date: action})
            else:
                print('Invalid operation')
            with open(json_file, 'w', encoding='utf-8') as file:
                json.dump(data, file, indent=2, ensure_ascii=False)
        except ValueError:
            break
    print('after trig calc', data)


def full_calc():
    # Функция полного калькулятора (простой, тригонометрический, курс и обмен валюты, просмотр истории)
    ex = ' '
    while ex != '1':
        choice_calc = input(
            '< Простой калькулятор - введите 1 > '
            '< тригонометрический - введите 2 >'
            '< Курс валют и операции с ними - введите 3 >'
            '< История - введите 4 > >>> '
        )
        if choice_calc == '1':
            simple_calc()
        elif choice_calc == '2':
            trigonometric_calc()
        elif choice_calc == '3':
            exchange_rates()
            currency_exchange()
        elif choice_calc == '4':
            get_history()
        else:
            print('Invalid choice')
        ex = input('< Завершить программу - введите 1 > < Сменить калькулятор - введите любой символ >  >>> ')


def check_of_availability():
    # Функция проверки наличия пользователя в БД, даётся 3 попытки на вход
    global auth_flag, log, pas
    count = 3
    data = read_json(json_file)
    if len(data) == 0:
        print('База данных пуста!')
    else:
        while count:
            if auth_flag:
                break
            log = get_log()
            pas = get_pass()
            for dict_ in data:
                if dict_['login'] == log and dict_['password'] == pas:
                    print(f'Привет {log}')
                    auth_flag = True
                    break
            else:
                print('Неверный логин или пароль!')
            count -= 1
    return auth_flag


def data_to_dict(**kwargs):
    # Возвращает аргументы в виде словаря
    return kwargs


def exchange_rates():
    # Выводит в консоль актуальный курс валют
    print('Актуальный курс валют', end='\n')
    for dict_ in json_object_html:
        print(f"{dict_['ccy']} - покупка: {dict_['buy']} продажа: {dict_['sale']}")


def currency_exchange():
    # Произвадит операции обиена валюты и записывает их в json
    global date
    data = read_json(json_file)
    choice_curr = input(f"Выберите авлюту: {' '.join([i['ccy'] for i in json_object_html])} >>> ").upper()
    for i in json_object_html:
        if i['ccy'] == choice_curr:
            amount_of_ccy = float(input('Кол-во валюты >>> '))
            if choice_curr == 'BTC':
                operation = (f"{choice_curr}/USD: {round(amount_of_ccy * float(i['buy']), 2)} ****** "
                             f"USD/{choice_curr}: {round(amount_of_ccy / float(i['sale']), 2)}")
                result = f"Currency exchange: '{choice_curr}: {amount_of_ccy} >>> {operation}' "
                print(result)
                for i in data:
                    if i['login'] == log:
                        i['operation'].append({date: result})
                with open(json_file, 'w', encoding='utf-8') as file:
                    json.dump(data, file, indent=2, ensure_ascii=False)
                break
            else:
                operation = (f"{choice_curr}/UAH: {round(amount_of_ccy * float(i['buy']), 2)} ****** "
                             f"UAH/{choice_curr}: {round(amount_of_ccy / float(i['sale']), 2)}")
                result = f"Currency exchange: '{choice_curr}: {amount_of_ccy} >>> {operation}' "
                print(result)
                for i in data:
                    if i['login'] == log:
                        i['operation'].append({date: result})
                with open(json_file, 'w', encoding='utf-8') as file:
                    json.dump(data, file, indent=2, ensure_ascii=False)
                break
    else:
        print('Нет такой валюты.')


def get_history():
    # Выводит в консоль историю операций авторизарованного пользователя
    data = read_json(json_file)
    _ = input('< Показать всю историю операций - 1 > < Показать последнюю операцию - 2 > ')
    for dict_ in data:
        if dict_['login'] == log:
            for oper in dict_['operation']:
                if len(dict_['operation']) == 0:
                    print('Пока ничего нет')
                else:
                    if _ == '1':
                        print(oper)
                    elif _ == '2':
                        print(dict_['operation'][-1])
                        break
                    else:
                        print('Неправильный запрос.')
                        break


def main():
    inp = input('Вход-введи 1, регистрация-введи 2 >>> ')
    if inp == '2':
        register()
        if auth_flag:
            print(f'Добро пожаловать {log}!')
            full_calc()
    elif inp == '1':
        check_of_availability()
        if not auth_flag:
            print('Вам доступен только простой калькулятор.')
            simple_calc()
        elif auth_flag:
            full_calc()
    else:
        print('Вам доступен только простой калькулятор.')
        simple_calc()


if __name__ == '__main__':
    try:
        main()
    except ZeroDivisionError:
        print('Деление на ноль!')
    except Exception as e:
        print(e)
